var profile = require("./profile"); //Must include the path. Don't need the file extension.
// "./" means current directory
var users = process.argv.slice(2);
//the console input is stored as an array.
//when we type in the console "node app.js username username etc...,
//element 0 is "node", element 1 is "app.js" and elements 2-n are the names we want
//so we call slice on that array at position 2 to create a new array and store it as "users"
users.forEach(profile.get);
 //not supported by older browsers, but it's fine in node



