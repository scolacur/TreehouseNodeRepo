//Problem: We need a simple way to look at a user's badge count and JavaScript points
//Solution: Use Node.js to connect to Treehouse's API to get profile information to print out

//we must "require" these modules before we can use them
//good to use the name of the module for clarity
var http = require("http"); 


//Print out message
function printMessage(username, badgeCount, points){
  var message = username + " has " + badgeCount + " total badge(s) and " + points + " points in JavaScript.";
  console.log(message);
}

//Print out error messages
function printError(e){
  console.error(e.message);
};

function get(username){
  //Connect to the API URL (http://teamtreehouse.com/username.json)
  var request = http.get("http://teamtreehouse.com/" + username + ".json", function(response){
    var body = "";
    //Read the data
    //Node sends things in streams/packets, so this 'data' event is going to happen more than once
    //So, we must concatenate the results of each event (or chunks) into one var
    response.on('data', function (chunk) {
      body += chunk;
    });
    //once all the packages are sent, node sends an end signal
    //lets log the body only once all the packages are sent
    response.on('end', function(){
      if (response.statusCode == 200) {
        try {
          //Parse the data
          var profile = JSON.parse(body);
          //Print the data
          printMessage(username, profile.badges.length, profile.points.JavaScript);
        } catch(error) {
          //Parse Error
            printError(error);
          }
      }
      else {
        //Status Code Error
        printError({message: "There was an error getting the profile for " + username + ". (" +     http.STATUS_CODES[response.statusCode] + ")"});
      }
    });
  }); //end of the request function
  
  //Connection Error
  request.on("error", printError); 
}

//For this module we've created, we want to export a function called "get" and assign it to the get function we've created. Again, it's good practice to keep these the same name.
module.exports.get = get;
